<?php get_header(); ?>
<div class="content">
   <div class="container">
      <h1><span>404</span> error</h1>
      <p>Sorry, this page does not exist or has been removed.</p>
   </div>
</div>
<?php get_footer(); ?>

