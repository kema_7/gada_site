</div>
<footer>
   <div class="container">
        <div class="footer_content flex">
            <div class="bottom_menu flex">
                <nav>
                    <h4>About us:</h4>
	                <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location' => 'main_menu2')); ?>
                </nav>
                <nav>
                    <h4>Contact:</h4>
		            <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location' => 'main_menu3')); ?>
                </nav>
                <nav>
                    <h4>Donate</h4>
		            <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location' => 'main_menu4')); ?>
                </nav>
            </div>
            <div class="logo_bottom">
                <a class="logo" href="<?php echo get_option('home') ?>">
                    <img src="/wp-content/themes/sociable/images/logo_white.png" alt="logo">
                </a>
            </div>
        </div>
   </div>
    <div class="the_end">
		<?php the_field('bottom_copy','options'); ?>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>