<?php get_header(); ?>
   <div class="content">
      <div class="container">
            <?php if($top_slider = get_field('top_slider')) { ?>
                <div class="top_slider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php foreach ($top_slider as $ts ) { ?>
                                <div class="swiper-slide cover" style="background: url('<?php echo $ts['image'];?>') no-repeat; background-size: cover;">
                                    <div class="text">
                                        <?php echo $ts['text']; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            <?php } ?>


          <div class="middle">
                  <div class="motto">
                      <h2><?php the_field('motto'); ?></h2>
                  </div>

          </div>


	          <?php if($front_items = get_field('front_items')) { ?>
                  <div class="front_items flex">
			          <?php foreach($front_items as $front_item) { ?>
                          <div class="item cover" style="background: url('<?php echo $front_item['image'];?>') no-repeat; background-size: cover;" >
                              <div class="content_item">
                                  <div class="top_title">
							          <?php echo $front_item['text']; ?>
                                  </div>
                                  <a href="<?php echo $front_item['link']; ?>" class="button">Read more</a>
                              </div>
                          </div>
			          <?php } ?>
                  </div>
	          <?php } ?>
          </div>


   </div>
<?php get_footer(); ?>