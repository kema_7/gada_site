<?php
header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
header('Content-Type: text/html; charset=utf-8');
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header('X-UA-Compatible: IE=Edge');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title><?php seo_title(); ?></title>
   <meta name="MobileOptimized" content="width"/>
   <meta name="HandheldFriendly" content="True"/>
   <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
   <meta name="viewport" content="initial-scale=1.0, width=device-width">
   <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="main">
   <header>
      <div class="top_header">
          <div class="container flex">
              <a class="logo" href="<?php echo get_option('home') ?>">
                  <img src="/wp-content/themes/sociable/images/logo.png" alt="logo">
              </a>
              <div class="menues flex">
                  <div id="menuOpen">
                      <a class="logo_responsive" href="<?php echo get_option('home') ?>">
                          <img src="/wp-content/themes/sociable/images/logo_white.png" alt="logo">
                      </a>
                      <button class="hamburger hamburger--collapse" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
                      </button>
                  </div>
                  <nav id="mainMenu">
		              <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location' => 'main_menu')); ?>
                  </nav>
              </div>
          </div>
      </div>
   </header>