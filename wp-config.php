<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gada');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'LV9r7M*h}elO*QP]*Smw9l}03T_jH#Pz$7BCf>63Uwwsu 2e^{|sc4INhr^?6C .');
define('SECURE_AUTH_KEY',  'h);uL:hS<@;D:,V>Nx@)|+9@XZaHBm3j:L7<cLAWYLf=.34Z1MN0p5Bs&g}RcuSb');
define('LOGGED_IN_KEY',    'YEI:%)#o6x*qC8vR|)R1=X6DsPcgu9-*+`:$kW<8dTNR:OS3*E p`sPZu{/%5Js|');
define('NONCE_KEY',        'rn7QhcsI(J[1a!mssXdg:Tbj(LtyD!:y_kOq0&Q^&^6E0(RYY.B4fb!:erqFfTB<');
define('AUTH_SALT',        'X3%]mFj}@k*[l[6?RW2nO7R/}C,#YDMmRe[Q`lx3|[g$hPucv<QRVC0f[zTbm4&w');
define('SECURE_AUTH_SALT', '1;s^Pn:yurB#TQpu0Bb/db$2Dukx_n7C}goK-a^dUXit3Odi_-zK(,[}Uf3|J=jz');
define('LOGGED_IN_SALT',   '*F. {?ZY_Y,TC{3_oY;SJ(T0+WdP[3E?$GpxsXf}mKv[ozN!wY,Tw>xN_<%IQXx0');
define('NONCE_SALT',       '(;ckC<gdu2M-,&,nd1<U3Dy5xf6qu=HZ+kmyC(cqG3g6Mh4v5RKiINiVvj{yUe~n');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
